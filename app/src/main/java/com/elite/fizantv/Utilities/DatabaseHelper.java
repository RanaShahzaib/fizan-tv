package com.elite.fizantv.Utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.elite.fizantv.Adapters.Channel;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "tv.db";
    private static String TABLE_NAME = "channels";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_LINK = "link";
    private static final String COLUMN_THUMB = "thumb";
    private static final String COLUMN_CATEGORY = "category";
    private static final String COLUMN_VIEWS = "views";
    private static final String COLUMN_ID  = "id";


    private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" ("+
            COLUMN_ID+ " INTEGER,"+
            COLUMN_NAME+" TEXT,"+
            COLUMN_LINK+" TEXT,"+
            COLUMN_THUMB+" TEXT,"+
            COLUMN_CATEGORY+" INTEGER," +
            COLUMN_VIEWS+" TEXT" +
            ")";

    private static int VERSION = 5;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public ArrayList<Channel> getChannels(int category){
        ArrayList<Channel> mList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query;
        if(category == 0){
            query = "SELECT * FROM "+TABLE_NAME;
        }else{
            query = "SELECT * FROM "+TABLE_NAME+" WHERE "+COLUMN_CATEGORY +" = "+category;
        }
        Cursor cursor = null;
        try{
            cursor = db.rawQuery(query, null);
            while(cursor.moveToNext()){
                mList.add(new Channel(cursor.getInt(0), cursor.getString(1), cursor.getString(3), cursor.getString(2), cursor.getInt(4), cursor.getString(5)));
                Log.v("Channel", "Id: "+cursor.getString(0)+" Category: "+cursor.getInt(3)+" Name:" +cursor.getString(1) );
            }
        }catch (Exception ex){
            Log.e("DB_Error", ex.getMessage());
        }finally {
            cursor.close();
        }
        Log.v("List Size", "Size : "+mList.size());
        return mList;
    }

    public void addChannels(ArrayList<Channel> mList){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL(CREATE_TABLE);
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            for (Channel channel:mList) {
                values.put(COLUMN_ID, channel.getId());
                values.put(COLUMN_NAME, channel.getChannelName());
                values.put(COLUMN_LINK, channel.getLink());
                values.put(COLUMN_THUMB, channel.getDrawable());
                values.put(COLUMN_CATEGORY, channel.getCategory());
                values.put(COLUMN_VIEWS, channel.getViews());
                db.insert(TABLE_NAME, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void updateChannel(Channel channel){
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
                values.put(COLUMN_ID, channel.getId());
                values.put(COLUMN_NAME, channel.getChannelName());
                values.put(COLUMN_LINK, channel.getLink());
                values.put(COLUMN_THUMB, channel.getDrawable());
                values.put(COLUMN_CATEGORY, channel.getCategory());
                values.put(COLUMN_VIEWS, channel.getViews());
                db.update(TABLE_NAME, values,COLUMN_ID+"="+channel.getId(), null);
        } finally {
            db.endTransaction();
            db.close();
        }
    }
}
