package com.elite.fizantv.Utilities

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by Shahzaib on 7/19/2018.
 */
class PreferencesHelper(var context: Context) {

    //Should show Ads
    fun shouldShowHomeAd(): Boolean{
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("show_home_ad", false)
    }
    fun shouldShowSportsAd(): Boolean{
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("show_sports_ad", false)
    }
    fun shouldShowPlayerAd(): Boolean{
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("show_player_ad", false)
    }
    fun shouldShowPlayerInterstitial(): Boolean{
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("show_interstitial", false)
    }

    //Set Should show Ads
    fun setShouldShowHomeAd(flag: Boolean){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("show_home_ad", flag).apply()
    }
    fun setShouldShowSportsAd(flag: Boolean){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("show_sports_ad", flag).apply()
    }
    fun setShouldShowPlayerAd(flag: Boolean){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("show_player_ad", flag).apply()
    }
    fun setShouldShowPlayerInterstitial(flag: Boolean){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("show_interstitial", flag).apply()
    }

    //Set Ad Units
    fun setHomeAdUnit(unit: String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("home_ad_unit", unit).apply()
    }
    fun setSportAdUnit(unit: String) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("sports_ad_unit", unit).apply()
    }
    fun setPlayerAdUnit(unit: String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("player_ad_unit", unit).apply()
    }
    fun setBannerAdUnit(unit: String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("banner_ad_unit", unit).apply()
    }
    fun setInterstitialAdUnit(unit: String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("interstitial_ad_unit", unit).apply()
    }
    fun setInterstitialAdUnit1(unit: String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("interstitial_ad_unit1", unit).apply()
    }

    //Get Ad Units
    fun getHomeAdUnit(): String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString("home_ad_unit", "null")
    }
    fun getSportAdUnit(): String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString("sports_ad_unit", "null")
    }
    fun getPlayerAdUnit(): String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString("player_ad_unit", "null")
    }
    fun getBannerAdUnit(): String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString("banner_ad_unit", "null")
    }
    fun getInterstitialUnit(): String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString("interstitial_ad_unit", "null")
    }
    fun getInterstitialUnit1(): String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString("interstitial_ad_unit", "null")
    }

    //Admob

    fun isAdmob():Boolean{
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("isAdmob", false)
    }

    fun setAdmob(flag: Boolean){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("isAdmob", flag).apply()
    }

}