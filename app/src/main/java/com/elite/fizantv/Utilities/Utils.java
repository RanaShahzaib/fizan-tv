package com.elite.fizantv.Utilities;

import android.content.Context;
import android.content.Intent;

import com.squareup.otto.Bus;

/**
 * Created by Rana Shahzaib on 11/19/2016.
 */

public class Utils {
    public static final String tag1 = "<video id=\"vidcontent\" width=\"100%\" height=\"100%\" autoplay poster=\"http://ufmo.asia/video_poster.jpg\">\n" +
            "  <source src=\"";
    public static final String tag2="\" type=\"video/mp4\">\n" +
            "  Your browser does not support HTML5 video.\n" +
            "</video>";

    public static final int ALL=0;
    public static final int NEWS=1;
    public static final int DRAMA=2;
    public static final int ENTERTAINMENT=3;
    public static final int REGIONAL=4;
    public static final int RELIGION=5;
    public static final int SPORTS=6;

    public static final String URL="http://wehostyourbusiness.net/api/getChannels";
    //http://watchnflhd.com/FizanTV/tv_version.php
    public static final String VERSION = "http://wehostyourbusiness.net/NewFizanTV/tv_version.php";

    public static final String REPORT_ISSUE = "http://wehostyourbusiness.net/NewFizanTV/report_issue.php";
    public static final String POST_USER_INFO = "http://wehostyourbusiness.net/api/postUserInfo";
    public static final String RELATED_CHANNELS = "http://wehostyourbusiness.net/api/getRelatedChannels";

    public static final String APP_PREFERENCE="fizan_tv";
    public static final String IS_ICON_CREATED="icon";

    public static void share(Context context, String msg){

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT,msg);
        intent.setType("text/plain");
        context.startActivity(intent);
    }

    public static String getCategory(int category){
        if(category==NEWS){
            return "News";
        }else if(category==DRAMA){
            return "Drama";
        }else if(category==ENTERTAINMENT){
            return "Entertainment";
        }else if(category==REGIONAL){
            return "Regional";
        }else if(category==RELIGION){
            return "Religion";
        }else if(category==SPORTS){
            return "Sports";
        }

        return "";
    }
    private static Bus bus = new Bus();

    public static Bus getBus(){
        return bus;
    }
}
