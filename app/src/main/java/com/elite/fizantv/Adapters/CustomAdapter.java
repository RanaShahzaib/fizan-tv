package com.elite.fizantv.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.elite.fizantv.ChannelActivity;
import com.elite.fizantv.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Rana Shahzaib on 11/25/2016.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<Channel> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail;
        public Button watch;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.channel_name);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            watch= (Button) view.findViewById(R.id.watch_now);
        }
    }


    public CustomAdapter(Context mContext, ArrayList<Channel> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.channel_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Channel channel = albumList.get(position);
        holder.title.setText(channel.getChannelName());
        Picasso.get().load(channel
                .getDrawable())
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(holder.thumbnail);
        holder.watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                watch(position);
            }
        });
        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                watch(position);
            }
        });
    }

    void watch(int position){
       Channel channel = albumList.get(position);
       Intent intent = new Intent(mContext, ChannelActivity.class);
       intent.putExtra("channel_name", channel.getChannelName());
       intent.putExtra("channel_image", channel.getDrawable());
       intent.putExtra("channel_link", channel.getLink());
       intent.putExtra("channel_category", channel.getCategory());
        intent.putExtra("channel_views", channel.getViews());
        intent.putExtra("channel_id", channel.getId());
       mContext.startActivity(intent);
    }
    @Override
    public int getItemCount() {
        return albumList.size();
    }

}
