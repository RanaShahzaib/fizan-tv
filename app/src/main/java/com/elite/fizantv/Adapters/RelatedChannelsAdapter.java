package com.elite.fizantv.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeBannerAd;
import com.elite.fizantv.R;
import com.elite.fizantv.Utilities.Utils;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RelatedChannelsAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_AD = 1;
    private static final int VIEW_TYPE_CHANNEL = 2;

    ArrayList<Channel> mChannels;
    class ChannelViewHolder extends RecyclerView.ViewHolder{

        ImageView mChannelIcon;
        TextView mChannelName;
        TextView mChannelViews;

        ChannelViewHolder(View view){
            super(view);
            mChannelIcon = view.findViewById(R.id.channel_icon_view);
            mChannelViews = view.findViewById(R.id.channel_views);
            mChannelName = view.findViewById(R.id.channel_title);
        }
    }

    class AdViewHolder extends RecyclerView.ViewHolder{

        TextView nativeAdTitle;
        TextView nativeAdSocialContext;
        TextView sponsoredLabel;
        AdIconView nativeAdIconView;
        Button nativeAdCallToAction;
        NativeBannerAd nativeBannerAd;

        View adView;

        AdViewHolder(View adView){
            super(adView);
            nativeAdTitle = adView.findViewById(R.id.native_ad_title);
            nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
            nativeAdIconView = adView.findViewById(R.id.native_icon_view);
            nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);
            sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
            this.adView = adView;
        }

        void loadAd(){
            loadNativeBanner(adView);
        }

        void loadNativeBanner(final View adView){
            nativeBannerAd = new NativeBannerAd(adView.getContext(), "154577565377261_214380572730293");
            nativeBannerAd.setAdListener(new NativeAdListener() {
                @Override
                public void onMediaDownloaded(Ad ad) { }
                @Override
                public void onError(Ad ad, AdError adError) { }
                @Override
                public void onAdLoaded(Ad ad) {
                    if (nativeBannerAd == null || nativeBannerAd != ad) {
                        return;
                    }
                    inflateAd(nativeBannerAd, adView);
                }

                @Override
                public void onAdClicked(Ad ad) {}
                @Override
                public void onLoggingImpression(Ad ad) {}
            });
            nativeBannerAd.loadAd();
        }

        void inflateAd(NativeBannerAd nativeBannerAd,View adView){
            RelativeLayout adChoicesContainer = adView.findViewById(R.id.ad_choices_container);
            AdChoicesView adChoicesView = new AdChoicesView(adView.getContext(), nativeBannerAd, true);
            adChoicesContainer.addView(adChoicesView, 0);

            sponsoredLabel.setText(nativeBannerAd.getSponsoredTranslation());
            nativeAdCallToAction.setText(nativeBannerAd.getAdCallToAction());
            nativeAdCallToAction.setVisibility(
                    nativeBannerAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
            nativeAdTitle.setText(nativeBannerAd.getAdvertiserName());
            nativeAdSocialContext.setText(nativeBannerAd.getAdSocialContext());
            // Register the Title and CTA button to listen for clicks.

            List<View> clickableViews = new ArrayList<>();
            clickableViews.add(nativeAdTitle);
            clickableViews.add(nativeAdCallToAction);
            nativeBannerAd.registerViewForInteraction(adView, nativeAdIconView, clickableViews);
        }
    }

    public RelatedChannelsAdapter(ArrayList<Channel> mList){
        mChannels = mList;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder1, int i) {
        final Channel channel = mChannels.get(i);
        if(VIEW_TYPE_AD == getItemViewType(i)){
            AdViewHolder viewHolder = (AdViewHolder)viewHolder1;
            viewHolder.loadAd();
        }else{
            ChannelViewHolder viewHolder = (ChannelViewHolder)viewHolder1;
            viewHolder.mChannelName.setText(channel.getChannelName());
            viewHolder.mChannelViews.setText(channel.getViews()+" - Views");
            Picasso.get().load(channel
                    .getDrawable())
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(viewHolder.mChannelIcon);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getBus().post(channel);
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder holder;
        if(VIEW_TYPE_AD == i){
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.related_channel_ad_container,viewGroup, false);
            holder = new AdViewHolder(view);
        }else{
            holder = new ChannelViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.related_channel_layout,viewGroup, false));
        }
        return holder;
    }

    @Override
    public int getItemCount() {
        return mChannels.size();
    }

    @Override
    public int getItemViewType(int position) {
        //return mChannels.get(position).getChannelName()==null?VIEW_TYPE_AD:VIEW_TYPE_CHANNEL;
        return VIEW_TYPE_CHANNEL;
    }

    public void clear(){
        mChannels.clear();
        notifyDataSetChanged();
    }
}
