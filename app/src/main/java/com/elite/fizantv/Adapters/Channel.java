package com.elite.fizantv.Adapters;

import java.io.Serializable;

/**
 * Created by Rana Shahzaib on 11/20/2016.
 */

public class Channel implements Serializable{
    private String mThumb;
    private String mName;
    private String mLink;
    private String mViews;
    private int mCategory;
    private int id;

    public Channel(String name, String thumb, String link){
        this.mThumb = thumb;
        this.mName = name;
        this.mLink = link;
    }

    public Channel(int id, String name, String thumb, String link, int category){
        this.mThumb = thumb;
        this.mName = name;
        this.mLink = link;
        this.id = id;
        this.mCategory = category;
    }

    public Channel(int id, String name, String thumb, String link, int category, String views){
        this.mThumb = thumb;
        this.mName = name;
        this.mLink = link;
        this.id = id;
        this.mCategory = category;
        this.mViews = views;
    }

    public Channel(){

    }

    public String getDrawable() {
        return mThumb;
    }

    public void setDrawable(String thumb) {
        this.mThumb = thumb;
    }

    public String getChannelName() {
        return mName;
    }

    public void setChannelName(String name) {
        this.mName = name;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        this.mLink = link;
    }

    public void setCategory(int category){
        this.mCategory = category;
    }

    public int getCategory(){
        return mCategory;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public String getViews() {
        return mViews;
    }

    public void setViews(String mViews) {
        this.mViews = mViews;
    }
}
