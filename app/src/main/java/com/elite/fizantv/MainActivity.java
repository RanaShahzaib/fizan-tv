package com.elite.fizantv;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.applovin.sdk.AppLovinSdk;
import com.codemybrainsout.ratingdialog.RatingDialog;
import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeAdViewAttributes;
import com.facebook.ads.NativeBannerAd;
import com.facebook.ads.NativeBannerAdView;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.iid.FirebaseInstanceId;
import com.elite.fizantv.Adapters.Channel;
import com.elite.fizantv.Adapters.CustomAdapter;
import com.elite.fizantv.Adapters.GridSpacingItemDecoration;
import com.elite.fizantv.Adapters.ViewPagerAdapter;
import com.elite.fizantv.Fragments.AllChannels;
import com.elite.fizantv.Fragments.Drama;
import com.elite.fizantv.Fragments.Entertainment;
import com.elite.fizantv.Fragments.News;
import com.elite.fizantv.Fragments.Regional;
import com.elite.fizantv.Fragments.Religion;
import com.elite.fizantv.Fragments.Sports;
import com.elite.fizantv.Utilities.DatabaseHelper;
import com.elite.fizantv.Utilities.PreferencesHelper;
import com.elite.fizantv.Utilities.Utils;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.squareup.otto.Subscribe;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    SmartTabLayout tabLayout;
    ViewPager viewPager;
    private static final String TAG = MainActivity.class.getSimpleName();
    private MaterialSearchView searchView;
    //private View search_view;
    private ArrayList<Channel> searchedChannels = new ArrayList<>();
    private CustomAdapter searchAdapter;
    private RecyclerView searchRecyclerView;
    DatabaseHelper mDBHelper;
    private AdView adView;
    LinearLayout mNativeAdContainer;
    NativeAdViewAttributes mAdViewAttributes = new NativeAdViewAttributes();
    NativeAd nativeAd;

    String BANNER[] = {"154577565377261_174984510003233", "154577565377261_178565999645084", "154577565377261_174984510003233"};

    String msg = "Watch 80+ TV channels live with Fizan TV. \nhttps://play.google.com/store/apps/details?id=com.pacificlive.tv1";

    PreferencesHelper mHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadExitAd();
        AppLovinSdk.initializeSdk(this);
        mHelper = new PreferencesHelper(this);
        MobileAds.initialize(this, getString(R.string.app_id));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        int i = new Random().nextInt(2);
        com.google.android.gms.ads.AdView adViewAM = findViewById(R.id.adView);
        if(!mHelper.isAdmob()){
            adViewAM.setVisibility(View.GONE);
            adView = new AdView(this,  mHelper.getBannerAdUnit(), AdSize.BANNER_HEIGHT_50);
            final LinearLayout adContainer = (LinearLayout) findViewById(R.id.banner_container);
            adContainer.addView(adView);
            adView.loadAd();
        }else{
            adViewAM.loadAd(new AdRequest.Builder().build());
        }


        mDBHelper = new DatabaseHelper(MainActivity.this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //mNativeAdContainer = (LinearLayout) findViewById(R.id.native_banner_container);
        searchAdapter = new CustomAdapter(this,searchedChannels);
        searchRecyclerView = (RecyclerView)findViewById(R.id.search_recycler_view);
        //search_view = findViewById(R.id.search_layout);
        searchRecyclerView.setItemAnimator(new DefaultItemAnimator());
        searchRecyclerView.setAdapter(searchAdapter);
        GridLayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        searchRecyclerView.setLayoutManager(mLayoutManager);
        searchRecyclerView.addItemDecoration(new GridSpacingItemDecoration(10, dpToPx(2), true));
        tabLayout = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        searchView = (MaterialSearchView)findViewById(R.id.search_view);
        //search_view.setVisibility(View.VISIBLE);
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                if(searchRecyclerView.getVisibility() == View.INVISIBLE){
                    //search_view.setVisibility(View.VISIBLE);
                    searchRecyclerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSearchViewClosed() {
                if(searchRecyclerView.getVisibility() == View.VISIBLE){
                    searchRecyclerView.setVisibility(View.INVISIBLE);
                }
            }
        });
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                View view = MainActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //
                if(newText.length()>0){
                    //searchedChannels.clear();
                    searchAndDisplay(newText);
                }
                return false;
            }
        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if(mHelper.shouldShowHomeAd()){
            requestNativeAd(mHelper.getHomeAdUnit(), 0);
        }
        loadChannels();
        postUserInfo();
    }

    private void loadNativeBanner(String id, final int retryCount){
        if(retryCount<2){
            final NativeBannerAd nativeBannerAd = new NativeBannerAd(this, id);
            nativeBannerAd.setAdListener(new NativeAdListener() {

                @Override
                public void onMediaDownloaded(Ad ad) {

                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    loadNativeBanner("154577565377261_214382302730120", retryCount+1);
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    // Race condition, load() called again before last ad was displayed
                    if (nativeBannerAd == null || nativeBannerAd != ad) {
                        return;
                    }
                    inflateBanner(nativeBannerAd);
                }

                @Override
                public void onAdClicked(Ad ad) {

                }

                @Override
                public void onLoggingImpression(Ad ad) {

                }
            });
            // load the ad
            nativeBannerAd.loadAd();
        }
    }

    private void inflateBanner(NativeBannerAd mNativeBannerAd){
        if (mNativeBannerAd != null && mNativeBannerAd.isAdLoaded()) {
            mNativeAdContainer.removeAllViews();

            // Create a NativeAdViewAttributes object (e.g. mAdViewAttributes)
            //   to render the native ads
            // Set the attributes
            mAdViewAttributes.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            mAdViewAttributes.setTitleTextColor(Color.WHITE);
            mAdViewAttributes.setDescriptionTextColor(Color.WHITE);
            mAdViewAttributes.setButtonBorderColor(R.color.colorPrimary);
            mAdViewAttributes.setButtonTextColor(Color.WHITE);

            mAdViewAttributes.setButtonColor(R.color.colorPrimary);
            // Use NativeAdView.render to generate the ad View
            // NativeAdViewType mViewType = NativeAdViewType.HEIGHT_100;
            View adView = NativeBannerAdView.render(
                    MainActivity.this,
                    mNativeBannerAd,
                    NativeBannerAdView.Type.HEIGHT_120,
                    mAdViewAttributes);

            // Add adView to the container showing Ads
            mNativeAdContainer.addView(adView, 0);
            mNativeAdContainer.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void loadChannels(){
        final ArrayList<Channel> list = new ArrayList<>();
        AndroidNetworking.post(Utils.URL)
                .addBodyParameter("c",String.valueOf(Utils.ALL))
                .setTag("LoadChannels")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        JSONArray array;
                        try{
                            array = new JSONArray(response);
                            for(int i=0;i<array.length();i++){
                                JSONObject object = array.getJSONObject(i);
                                Channel channel = new Channel(
                                        object.getString("name"),
                                        "http://wehostyourbusiness.net/"+object.getString("image"),
                                        object.getString("link")
                                );
                                channel.setId(object.getInt("id"));
                                channel.setCategory(object.getInt("category_id"));
                                channel.setViews(object.getString("views"));
                                list.add(channel);
                            }
                            Log.d("Main", "Here Above");
                            mDBHelper.addChannels(list);
                            Log.d("Main", "Here Below");
                            setupViewPager(viewPager);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        if(mDBHelper.getChannels(0).size()==0){
                            new MaterialStyledDialog.Builder(MainActivity.this)
                                    .setTitle("Error")
                                    .setDescription("Problem occurred while connecting to server")
                                    .setIcon(android.R.drawable.stat_notify_error)
                                    .setHeaderColor(R.color.colorPrimary)
                                    .setPositiveText("Retry")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            loadChannels();
                                        }
                                    })
                                    .setNegativeText("Cancel")
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            finish();
                                        }
                                    })
                                    .setCancelable(false)
                                    .show();
                        }else{
                            setupViewPager(viewPager);
                        }
                    }
                });

    }


    @Override
    protected void onResume() {
        super.onResume();
        Utils.getBus().register(this);
    }

    @Subscribe
    public void onExit(boolean exit){
        if (exit){
            finish();
        }
    }
    void setupViewPager(ViewPager viewPager) {
        Log.d("DebugFunction","Jere");
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AllChannels(), "All Channels");
        adapter.addFragment(new News(), "News");
        adapter.addFragment(new Drama(), "Drama");
        adapter.addFragment(new Entertainment(), "Entertainment");
        adapter.addFragment(new Regional(), "Regional");
        adapter.addFragment(new Religion(), "Religion");
        adapter.addFragment(new Sports(),"Sports");
        viewPager.setAdapter(adapter);
        tabLayout.setViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        }else {
            showDialog();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        if (id == R.id.nav_all_channels) {
            viewPager.setCurrentItem(0, true);
        } else if (id == R.id.nav_sports) {
            viewPager.setCurrentItem(6, true);
        } else if (id == R.id.nav_news) {
            viewPager.setCurrentItem(1, true);
        } else if (id == R.id.nav_drama) {
            viewPager.setCurrentItem(2, true);
        } else if (id == R.id.nav_entertainment) {
            viewPager.setCurrentItem(3, true);
        } else if (id == R.id.nav_regional) {
            viewPager.setCurrentItem(4, true);
        } else if (id == R.id.nav_religion) {
            viewPager.setCurrentItem(5, true);
        }else if(id == R.id.nav_exit){
            showDialog();
        }else if(id == R.id.nav_share){
            Utils.share(this, msg);
        }
        return true;
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public void showDialog() {

        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .icon(ResourcesCompat.getDrawable(getResources(),R.drawable.app_icon, null))
                .session(3)
                .threshold(3)
                .title("Would you like to rate this cool app?")
                .titleTextColor(R.color.black)
                .positiveButtonText("Not Now")
                .negativeButtonText("Never")
                .positiveButtonTextColor(R.color.colorPrimary)
                .negativeButtonTextColor(R.color.grey_500)
                .formTitle("Submit Feedback")
                .formHint("Tell us where we can improve")
                .formSubmitText("Submit")
                .formCancelText("Cancel")
                .onThresholdCleared(new RatingDialog.Builder.RatingThresholdClearedListener() {
                    @Override
                    public void onThresholdCleared(RatingDialog ratingDialog, float rating, boolean thresholdCleared) {
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                        ratingDialog.dismiss();
                    }
                })
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {
                        uploadFeedback(feedback);
                    }
                }).build();
        ratingDialog.show();

        if(!ratingDialog.isShowing()&&!nativeAd.isAdLoaded()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Exit");

            builder.setMessage("Are you really want to exit?");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    loadExitAd();
                }
            });
            builder.show();
        }else if(!ratingDialog.isShowing()&&nativeAd.isAdLoaded()){
            MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(this);
            dialogBuilder.customView(R.layout.exit_dialog_layout, false)
                    .positiveText("Exit")
                    .negativeText("Cancel")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            loadExitAd();
                        }
                    });

            MaterialDialog dialog = dialogBuilder.build();
            View adView = dialog.getView();
            AdIconView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
            TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
            MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
            TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
            TextView adBody = adView.findViewById(R.id.native_ad_body);
            Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

            // Set the Text.
            nativeAdTitle.setText(nativeAd.getAdvertiserName());

            nativeAdCallToAction.setVisibility( (nativeAd.hasCallToAction())? View.VISIBLE : View.INVISIBLE);
            nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
            adBody.setText(nativeAd.getAdBodyText());
            sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

            // Create a list of clickable views
            ArrayList clickableViews = new ArrayList<View>();
            clickableViews.add(nativeAdTitle);
            clickableViews.add(nativeAdCallToAction);

            // Register the Title and CTA button to listen for clicks.
            nativeAd.registerViewForInteraction(
                    adView,
                    nativeAdMedia,
                    nativeAdIcon,
                    clickableViews);
            dialog.show();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }
    public void searchAndDisplay(String text){
        if(AllChannels.list!=null&&AllChannels.list.size()>0){
            ArrayList<Channel> allChannels = AllChannels.list;
            searchedChannels.clear();
            searchAdapter.notifyDataSetChanged();
            for(int i = 0;i < allChannels.size(); i++){
                if(allChannels.get(i).getChannelName().toLowerCase().contains(text.toLowerCase())){
                    if(!searchedChannels.contains(allChannels.get(i)) ){
                        searchedChannels.add(allChannels.get(i));
                        searchAdapter.notifyItemInserted(searchedChannels.size()-1);
                    }
                }
            }

        }
    }

    void requestNativeAd(String id, final int retryCount){
        if(retryCount<2){
            final NativeAd nativeAd = new NativeAd(this, id);
            nativeAd.setAdListener(new NativeAdListener() {
                @Override
                public void onMediaDownloaded(Ad ad) {
                    // Native ad finished downloading all assets
                    Log.e(TAG, "Native ad finished downloading all assets.");
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    requestNativeAd("154577565377261_214065089428508", retryCount+1);
                    Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    // Native ad is loaded and ready to be displayed
                    Log.d(TAG, "Native ad is loaded and ready to be displayed!");
                    inflateNativeAd(nativeAd);
                }

                @Override
                public void onAdClicked(Ad ad) {
                    // Native ad clicked
                    Log.d(TAG, "Native ad clicked!");
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    // Native ad impression
                    Log.d(TAG, "Native ad impression logged!");
                }
            });

            // Request an ad
            nativeAd.loadAd();
        }else{
        }
    }
    void inflateNativeAd(final NativeAd nativeAd){
        AlertDialog.Builder builder = new AlertDialog.Builder(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.native_ad_dialog, null);

        dialogView.findViewById(R.id.ad_unit).setVisibility(View.GONE);
        new android.os.Handler().post(new Runnable() {
            @Override
            public void run() {
                dialogView.findViewById(R.id.ad_unit).setVisibility(View.VISIBLE);
                LinearLayout adChoicesContainer = dialogView.findViewById(R.id.ad_choices_container);
                AdChoicesView adChoicesView = new AdChoicesView(MainActivity.this, nativeAd, true);
                adChoicesContainer.addView(adChoicesView, 0);

                // Create native UI using the ad metadata.
                AdIconView nativeAdIcon = dialogView.findViewById(R.id.native_ad_icon);
                TextView nativeAdTitle = dialogView.findViewById(R.id.native_ad_title);
                MediaView nativeAdMedia = dialogView.findViewById(R.id.native_ad_media);
                TextView nativeAdSocialContext = dialogView.findViewById(R.id.native_ad_social_context);
                TextView nativeAdBody = dialogView.findViewById(R.id.native_ad_body);
                TextView sponsoredLabel = dialogView.findViewById(R.id.native_ad_sponsored_label);
                Button nativeAdCallToAction = dialogView.findViewById(R.id.native_ad_call_to_action);

                // Set the Text.
                nativeAdTitle.setText(nativeAd.getAdvertiserName());
                nativeAdBody.setText(nativeAd.getAdBodyText());
                nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
                sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                // Create a list of clickable views
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(nativeAdTitle);
                clickableViews.add(nativeAdCallToAction);

                // Register the Title and CTA button to listen for clicks.
                nativeAd.registerViewForInteraction(
                        dialogView,
                        nativeAdMedia,
                        nativeAdIcon,
                        clickableViews);
            }
        });

        builder.setView(dialogView);
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        View close = dialogView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if(!isFinishing()){
            dialog.show();
        }
    }

    void postUserInfo(){
        AndroidNetworking.post(Utils.POST_USER_INFO)
                .addBodyParameter("device",android.os.Build.MODEL)
                .addBodyParameter("fcm_id",getFcmId())
                .addBodyParameter("token",Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))
                .setTag("PostUserInfo")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    String getFcmId(){
        try{
            return FirebaseInstanceId.getInstance().getToken();
        }catch (Exception ex){
            return "";
        }
    }

    public void onPause() {
        super.onPause();
        Utils.getBus().unregister(this);
    }

    void uploadFeedback(String feedback){
        AndroidNetworking.post(Utils.REPORT_ISSUE)
                .addBodyParameter("token",Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))
                .addBodyParameter("complaint", feedback)
                .setTag("Feedback")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(getApplication()!=null){
                            Toast.makeText(getApplicationContext(), "Thanks for helping us to improve this app.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {}
                });
    }

    void loadExitAd(){
        nativeAd = new NativeAd(this, "587052361741089_587056875073971");
        nativeAd.loadAd();
    }

}

