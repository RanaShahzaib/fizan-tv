package com.elite.fizantv.Fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.elite.fizantv.Adapters.Channel;
import com.elite.fizantv.Adapters.CustomAdapter;
import com.elite.fizantv.Adapters.GridSpacingItemDecoration;
import com.elite.fizantv.R;
import com.elite.fizantv.Utilities.DatabaseHelper;
import com.elite.fizantv.Utilities.Utils;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Religion extends Fragment {
    RecyclerView recyclerView;
    CustomAdapter adapter;
    ArrayList<Channel> list;
    ProgressBar mProgress;

    public Religion() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_religion,container,false);
        list = new ArrayList<Channel>();
        adapter = new CustomAdapter(getActivity(),list);
        addChannels();
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(10, dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        mProgress = (ProgressBar)view.findViewById(R.id.progress);
        mProgress.setVisibility(View.GONE);
        return view;
    }

    public void addChannels(){
        DatabaseHelper mDBHelper = new DatabaseHelper(getContext());
        list = mDBHelper.getChannels(Utils.RELIGION);
        adapter = new CustomAdapter(getActivity(),list);
        adapter.notifyDataSetChanged();
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
