package com.elite.fizantv.Fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.elite.fizantv.Adapters.Channel;
import com.elite.fizantv.Adapters.CustomAdapter;
import com.elite.fizantv.Adapters.GridSpacingItemDecoration;
import com.elite.fizantv.R;
import com.elite.fizantv.Utilities.DatabaseHelper;
import com.elite.fizantv.Utilities.Utils;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AllChannels extends Fragment {
    RecyclerView recyclerView;
    CustomAdapter adapter;
    ProgressBar mProgress;

    public AllChannels() {
        // Required empty public constructor
    }
    RecyclerView.LayoutManager mLayoutManager;
    public static ArrayList<Channel> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_channels, container, false);
        list = new ArrayList<Channel>();
        addChannels();
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        mProgress = (ProgressBar)view.findViewById(R.id.progress);
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(10, dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        mProgress.setVisibility(View.GONE);
        return view;
    }
    public void addChannels(){
//        final StringRequest request = new StringRequest(Request.Method.POST, Utils.URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                mProgress.setVisibility(View.INVISIBLE);
//                JSONArray array ;
//                try{
//                    array = new JSONArray(response);
//                    for(int i=0;i<array.length();i++){
//                        JSONObject object = array.getJSONObject(i);
//                        Channel channel = new Channel(
//                                object.getString("name"),
//                                object.getString("thumb"),
//                                object.getString("link")
//                        );
//                        list.add(channel);
//                    }
//                    adapter.notifyDataSetChanged();
//                }catch (Exception ex){
//                    ex.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                if(AllChannels.this.getActivity()!=null){
//                    Toast.makeText(AllChannels.this.getActivity(),"Oops ! Some error occurred..", Toast.LENGTH_SHORT).show();
//                }
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String,String> map = new HashMap<>();
//                map.put("c",Utils.ALL);
//                return map;
//            }
//        };
//        RequestQueue queue = Volley.newRequestQueue(getActivity());
//        queue.add(request);
        DatabaseHelper mDBHelper = new DatabaseHelper(getContext());
        list = mDBHelper.getChannels(Utils.ALL);
        adapter = new CustomAdapter(getActivity(),list);
        adapter.notifyDataSetChanged();
    }


    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


}

