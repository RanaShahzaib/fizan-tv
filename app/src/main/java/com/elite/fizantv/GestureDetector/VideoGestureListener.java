package com.elite.fizantv.GestureDetector;

/**
 * Created by Rana Shahzaib on 2/2/2017.
 */

public interface VideoGestureListener {
    void onVerticalScroll(float percent, int direction);
}
