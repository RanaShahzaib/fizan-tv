package com.elite.fizantv

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.view.View.*
import android.webkit.*
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.applovin.adview.AppLovinInterstitialAd
import com.applovin.sdk.AppLovinAd
import com.applovin.sdk.AppLovinAdLoadListener
import com.applovin.sdk.AppLovinAdSize
import com.applovin.sdk.AppLovinSdk
import com.facebook.ads.*
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.offline.FilteringManifestParser
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylist
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistParser
import com.google.android.exoplayer2.source.hls.playlist.RenditionKey
import com.google.android.exoplayer2.trackselection.*
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.firebase.analytics.FirebaseAnalytics
import com.elite.fizantv.Adapters.Channel
import com.elite.fizantv.Adapters.RelatedChannelsAdapter
import com.elite.fizantv.Utilities.DatabaseHelper
import com.elite.fizantv.Utilities.DividerItemDecoration
import com.elite.fizantv.Utilities.PreferencesHelper
import com.elite.fizantv.Utilities.Utils
import com.squareup.otto.Subscribe
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_channel.*
import kotlinx.android.synthetic.main.error_occourred.*
import org.json.JSONArray

class ChannelActivity : AppCompatActivity() {

    var isControlsActive = false
    var isPlaying = false
    var playerListener: PlayerListener = PlayerListener()

    lateinit var progressBar: ProgressBar;
    lateinit var player: SimpleExoPlayer
    var retryCount = 0
    lateinit var mFullScreenDialog: Dialog;

    internal var bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
    internal var videoTrackSelectionFactory: TrackSelection.Factory = AdaptiveTrackSelection.Factory(bandwidthMeter)
    internal var trackSelector: TrackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

    lateinit var playerView: PlayerView

    var mExoPlayerFullscreen = false;
    lateinit var helper: PreferencesHelper
    lateinit var interstitialAd1: com.google.android.gms.ads.InterstitialAd
    private var interstitialAd: InterstitialAd? = null
    private val TAG = "PlayerActivity"
    internal var channel: Channel? = null
    lateinit var mPlayerControls: View;
    var mFirebaseAnalytics: FirebaseAnalytics? = null
    lateinit var bannerAd: AdView;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_channel)
        progressBar = findViewById(R.id.progress)
        interstitialAd1 = com.google.android.gms.ads.InterstitialAd(this@ChannelActivity)
        interstitialAd1.adUnitId = getString(R.string.interstitial_unit_id)
        initFullscreenDialog()
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)

        mPlayerControls = findViewById(R.id.playerControls);
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)
        playerView = findViewById(R.id.playerview)
        playerView.player = player
        title = "Playing"
        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
        var id = intent.extras.get("channel_id").toString()
        var views = intent.extras.get("channel_views").toString()
        var channelCategory = intent.extras.get("channel_category").toString()
        channel = Channel(id.toInt(), intent.getStringExtra("channel_name"),
                intent.getStringExtra("channel_image"),
                intent.getStringExtra("channel_link"),
                channelCategory.toInt(),
                views)
        DatabaseHelper(this).updateChannel(channel)
        playerContainer.setOnClickListener {
            if (!isControlsActive) {
                mPlayerControls.visibility = View.VISIBLE
                isControlsActive = true
                //hideControlsAfterIntervals(2000)
            } else {
                mPlayerControls.visibility = View.GONE
                isControlsActive = false
            }
        }

        playPause.setOnClickListener {
            if(isPlaying){
                isPlaying = false
                player.playWhenReady = false;
                playPause.setImageResource(R.drawable.ic_play_button_stream)
            }else{
                isPlaying =true
                player.playWhenReady = true;
                playPause.setImageResource(R.drawable.ic_pause)
            }
        }

        fullScreenSmallScreen.setOnClickListener {
            if(mExoPlayerFullscreen){
                closeFullscreenDialog()
            }else{
                openFullScreen()
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        fullScreen.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                openFullScreen()
            }

        })
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.addItemDecoration(DividerItemDecoration(this, R.drawable.divider))
        playVideo(channel!!)
        findViewById<Button>(R.id.retry).setOnClickListener { loadRelatedChannels(channel!!) }
        if (helper.shouldShowPlayerInterstitial()&&isM3U8(channel!!.link)) run {
            requestInterstitialAd(helper.getInterstitialUnit()
                    , 0) }
        loadBannerAd()
    }

    private fun initFullscreenDialog() {
        mFullScreenDialog = object : Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            override fun onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }

    fun playPrimaryStream(URL: String){
            val bandwidthMeter = DefaultBandwidthMeter()
            val dataSourceFactory = DefaultDataSourceFactory(this,
                    Util.getUserAgent(this, "Fizan TV"), bandwidthMeter)
            val videoSource = HlsMediaSource.Factory(dataSourceFactory)
                    .setPlaylistParser(
                            FilteringManifestParser<HlsPlaylist, RenditionKey>(
                                    HlsPlaylistParser(), null))
                    .createMediaSource(Uri.parse(URL))
            player.prepare(videoSource)
            player.addListener(playerListener)

            player.playWhenReady = true
    }
    fun closeFullscreenDialog() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        var parent = playerContainer.parent as ViewGroup
        parent.removeView(playerContainer)
        var touchListenerVar = touchListener as ViewGroup
        touchListenerVar.addView(playerContainer)
        mExoPlayerFullscreen = false
        mFullScreenDialog.dismiss()
        fullScreenSmallScreen.visibility = View.GONE
        if(isControlsActive){
            mPlayerControls.visibility = View.GONE
            isControlsActive = false
        }
    }

    fun openFullScreen(){
        var parent = playerContainer.parent as ViewGroup
        parent.removeView(playerContainer)
        mFullScreenDialog.addContentView(playerContainer, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        mExoPlayerFullscreen = true;
        mFullScreenDialog.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        mFullScreenDialog.show();
        fullScreenSmallScreen.visibility = View.VISIBLE
        fullScreenSmallScreen.setImageResource(R.drawable.ic_exit_fullscreen)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        if(isControlsActive){
            mPlayerControls.visibility = View.GONE
            isControlsActive = false
        }
    }


    inner class PlayerListener : Player.EventListener {
        override fun onTimelineChanged(timeline: Timeline, manifest: Any?, reason: Int) {}
        override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {}
        override fun onLoadingChanged(isLoading: Boolean) {
            if (isLoading) {
                runOnUiThread {
                    progress.visibility = View.VISIBLE
                    live_badge.visibility = View.GONE
                }
            } else {
                runOnUiThread {
                    live_badge.visibility = View.VISIBLE
                    progress.visibility = View.GONE
                }
            }
        }

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                SimpleExoPlayer.STATE_BUFFERING -> {
                    run {
                        progress.setVisibility(View.VISIBLE)
                        live_badge.visibility = View.GONE
                    }
                }
                SimpleExoPlayer.STATE_READY -> {
                    playPause.setImageResource(R.drawable.ic_pause)
                    runOnUiThread {
                        live_badge.visibility = View.VISIBLE
                        progress.visibility = View.GONE
                    }
                }
            }
        }

        override fun onRepeatModeChanged(repeatMode: Int) {

        }

        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {

        }

        override fun onPlayerError(error: ExoPlaybackException) {
            error.printStackTrace()
            runOnUiThread {
                if(retryCount<3){
                    destoryPlayer()
                    playVideo(channel!!)
                    retryCount++;
                }
                else if(!isFinishing){
                    playPause.setImageResource(R.drawable.ic_play_button_stream)
                    showError()
                }
            }
        }

        override fun onPositionDiscontinuity(reason: Int) {}
        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
        override fun onSeekProcessed() {}
    }


    fun showError(){
        progress.visibility = View.GONE
        live_badge.visibility = View.GONE
        player.stop()
        retryCount = 0;
        var builder = AlertDialog.Builder(this@ChannelActivity)
                .setMessage("Stream is currently not available")
                .setCancelable(false)
                .setPositiveButton("OK", object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                    }
                })
        builder.show()
    }


    fun pause() {
        if (isM3U8(channel!!.link)) {
            player.playWhenReady = false
        }
    }

    fun resume() {
        if (isM3U8(channel!!.link)) {
            player.playWhenReady = true
        }
    }


    public override fun onPause() {
        super.onPause()
        Utils.getBus().unregister(this)
        pause()
    }

    public override fun onResume() {
        super.onResume()
        resume()
        Utils.getBus().register(this)
    }



//    internal fun loadAsWebView(URL : String) {
//        webview.setInitialScale(1)
//        webview.setWebChromeClient(MyWebChromeClient())
//        webview.getSettings().allowFileAccess = true
//        webview.setWebViewClient(MyWebViewClient())
//        webview.getSettings().javaScriptEnabled = true
//        webview.getSettings().loadWithOverviewMode = true
//        webview.getSettings().useWideViewPort = true
//        val SDK_INT = android.os.Build.VERSION.SDK_INT
//        if (SDK_INT > 16) {
//            webview.getSettings().mediaPlaybackRequiresUserGesture = false
//        }
//        webview.setVerticalScrollBarEnabled(false)
//        webview.setHorizontalScrollBarEnabled(false)
//        webview.setBackgroundColor(Color.BLACK)
//        val displaymetrics = DisplayMetrics()
//        windowManager.defaultDisplay.getMetrics(displaymetrics)
//        webview.setOnTouchListener(View.OnTouchListener { v, event -> event.action == MotionEvent.ACTION_MOVE })
//        try {
//            playerview.visibility = View.GONE
//            playerControls.visibility = View.GONE
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
//
//        webview.loadUrl(URL)
//    }

    private class MyWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            view.loadUrl(request.url.toString())
            return true
        }
    }

    private inner class MyWebChromeClient : WebChromeClient() {

        override fun onProgressChanged(view: WebView, progress: Int) {
            if (!isM3U8(channel!!.getLink())) {
                if (progress == 100) {
                    this@ChannelActivity.progress.setVisibility(INVISIBLE)

                } else {
                    this@ChannelActivity.progress.setVisibility(VISIBLE)
                }
            }
        }

        override fun onConsoleMessage(cm: ConsoleMessage): Boolean {
            // Spit out lots of console messages here
            return true
        }
    }

    fun isM3U8(s: String): Boolean {
        return s.contains("m3u8")
    }

    internal fun playVideo(channel: Channel) {
        val picasso = Picasso.Builder(this)
                .listener { picasso, uri, exception ->
                    exception.printStackTrace()
                }
                .build()
        var bundle = Bundle()
        bundle.putString("ChannelPlay", channel.channelName)
        mFirebaseAnalytics?.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
        picasso.load(channel?.drawable).fit()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(thumbnail1)
        channel_name.text = channel?.channelName
        channel_category.text = Utils.getCategory(channel!!.category)
        channel_views.text = channel.views+" - Views"

        if (isM3U8(channel.getLink())) {
            playerView.visibility = View.VISIBLE
            //playerControls.visibility = View.VISIBLE
            playPrimaryStream(channel.link)
        } else {
            var intent = Intent(this@ChannelActivity, PlayerActivity::class.java)
            intent.putExtra("link", channel.link)
            startActivity(intent)
            finish()
        }
        loadRelatedChannels(channel)
    }

    fun destoryPlayer(){
        player.playWhenReady = false
    }

    @Subscribe fun onChannelSelected(channel: Channel){
        if (isM3U8(this@ChannelActivity.channel!!.link)){
            destoryPlayer()
        }
        playVideo(channel)
        this@ChannelActivity.channel = channel;
    }

    fun loadRelatedChannels(channel: Channel){
        progress1.visibility = VISIBLE
        var mChannels = ArrayList<Channel>()
        var adapter  = recyclerView.adapter as RelatedChannelsAdapter?
        adapter?.clear()
        if(error_layout.visibility == VISIBLE){
            error_layout.visibility = GONE
            recyclerView.visibility = VISIBLE
        }
        AndroidNetworking.post(Utils.RELATED_CHANNELS)
                .setTag("Splash")
                .addBodyParameter("id",channel.id.toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(object : JSONArrayRequestListener {
                    override fun onResponse(response: JSONArray?) {
                        progress1.visibility = GONE
                        if(error_layout.visibility == View.VISIBLE){
                            error_layout.visibility = GONE
                        }
                        for (i in 0 until response!!.length()){
                            var obj = response.getJSONObject(i)
//                            if (i%3==0&&i>0){
//                                mChannels.add(Channel())
//                            }
                            var channel = Channel(obj.getInt("id"),
                                    obj.getString("name"),
                                    "http://wehostyourbusiness.net/"+obj.getString("image"),
                                    obj.getString("link"),
                                    obj.getInt("category_id"),
                                    obj.getString("views"));

                            if(isM3U8(channel.link)){
                                mChannels.add(channel)
                            }
                        }
                        recyclerView.adapter = RelatedChannelsAdapter(mChannels)
                    }
                    override fun onError(anError: ANError?) {
                        progress1.visibility = GONE
                        error_layout.visibility = VISIBLE
                        recyclerView.visibility = GONE
                    }
                })
        helper = PreferencesHelper(this)
    }

    internal fun requestInterstitialAd(id: String, retryCount: Int) {
        if (retryCount < 2) {
            interstitialAd = InterstitialAd(this, id)
            interstitialAd!!.setAdListener(object : InterstitialAdListener {
                override fun onInterstitialDisplayed(ad: Ad) {}

                override fun onInterstitialDismissed(ad: Ad) {}

                override fun onError(ad: Ad, adError: AdError) {
                    requestInterstitialAd(helper.getInterstitialUnit1(), retryCount + 1)
                }

                override fun onAdLoaded(ad: Ad) {
                    interstitialAd!!.show()
                }

                override fun onAdClicked(ad: Ad) {}

                override fun onLoggingImpression(ad: Ad) {

                }
            })
            interstitialAd!!.loadAd()
        } else {
            var interstitialAdDialog = AppLovinInterstitialAd.create( AppLovinSdk.getInstance( this ), this );
            AppLovinSdk.getInstance(this).adService.loadNextAd(AppLovinAdSize.INTERSTITIAL, object : AppLovinAdLoadListener {
                override fun adReceived(p0: AppLovinAd?) {
                    interstitialAdDialog.showAndRender(p0)
                }

                override fun failedToReceiveAd(p0: Int) {
                    Log.e("AppLovin", "Error while loading Ad :"+p0)
                }
            })
        }
    }

    fun loadBannerAd(){
        bannerAd = AdView(this, "587052361741089_587056251740700", AdSize.BANNER_HEIGHT_50)
        val adContainer = findViewById<View>(R.id.adContainer) as LinearLayout
        adContainer.addView(bannerAd)
        bannerAd.loadAd()
    }

}