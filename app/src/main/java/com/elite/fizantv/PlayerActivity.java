package com.elite.fizantv;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdListener;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.gms.ads.AdRequest;
import com.elite.fizantv.GestureDetector.VideoGestureListener;
import com.elite.fizantv.GestureDetector.ViewGestureListener;
import com.elite.fizantv.Utilities.PreferencesHelper;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class PlayerActivity extends AppCompatActivity implements View.OnClickListener, VideoGestureListener {
    WebView webView;
    View mScreen;
    View upperPanel,lowerPanel;
    TextView mChannelName;
    boolean isControlsVisible = true;
    ProgressBar mProgress;
    boolean isPlaying = true;
    ImageView mBtn;
    GestureDetector mGestureDetector;
    float mCurBrightness = -1;
    int mCurVolume = -1;
    AudioManager mAudioManager;
    int mMaxVolume;
    TextView mControlsPer;
    ImageView mControlsIcon;
    View mCenterLayout;
    VerticalProgressBar mVolume;
    VerticalProgressBar mBrightness;
    int channelPosition;
    private InterstitialAd interstitialAd;

    private AdView adView;
    Handler mainHandler = new Handler();
    BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
    TrackSelection.Factory videoTrackSelectionFactory =
            new AdaptiveTrackSelection.Factory(bandwidthMeter);
    TrackSelector trackSelector =
            new DefaultTrackSelector(videoTrackSelectionFactory);
    SimpleExoPlayer player;
    PlayerView playerView;
    String HIGH = "154577565377261_204625680372449";
    String LOW_ = "154577565377261_178544256313925";
    boolean status = false;
    int retryCount = 0;
    private static final String TAG = "PlayerActivity";
    PreferencesHelper helper;

    com.google.android.gms.ads.InterstitialAd interstitialAd1;

//    View skipNext, skipBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        interstitialAd1 = new com.google.android.gms.ads.InterstitialAd(PlayerActivity.this);
        interstitialAd1.setAdUnitId(getString(R.string.interstitial_unit_id));
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        mScreen = findViewById(R.id.screen);
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        playerView = findViewById(R.id.playerview);
        playerView.setPlayer(player);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        mControlsIcon = (ImageView)findViewById(R.id.icon);
        mCenterLayout = findViewById(R.id.center_layout);
        upperPanel = findViewById(R.id.upper_panel);
        lowerPanel = findViewById(R.id.lower_panel);
        mChannelName = (TextView)findViewById(R.id.channel_name);
        mControlsPer = (TextView)findViewById(R.id.controls_per);
        mProgress = (ProgressBar)findViewById(R.id.progress);
        mVolume = (VerticalProgressBar)findViewById(R.id.volume);
        mVolume.setScaleX(3f);
        mBrightness = (VerticalProgressBar)findViewById(R.id.brightness);
        mBrightness.setScaleX(3f);
        status = true;
        webView=(WebView)findViewById(R.id.webview);
        mProgress.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, android.graphics.PorterDuff.Mode.MULTIPLY);
        mScreen.setOnClickListener(this);
        mBtn = (ImageView)findViewById(R.id.btn);
//        skipBack = findViewById(R.id.skip_back);
//        skipNext = findViewById(R.id.skip_next);
//
//        skipNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(channelPosition != mChannelList.size()-1){
//                    channelPosition++;
//                    if(isM3U8(mChannelList.get(channelPosition).getLink())){
//                        playVideo(channelPosition);
//                    }else{
//                        if(channelPosition != mChannelList.size()-1){
//                            playVideo(++channelPosition);
//                        }
//                    }
//                }
//            }
//        });
//
//        skipBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(channelPosition!=0){
//                    channelPosition--;
//                    if(isM3U8(mChannelList.get(channelPosition).getLink())){
//                        playVideo(channelPosition);
//                    }else{
//                        if(channelPosition != mChannelList.size()-1){
//                            playVideo(--channelPosition);
//                        }
//                    }
//                }
//            }
//        });


        channelPosition = getIntent().getIntExtra("position", 0);


//        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Utils.share(PlayerActivity.this, "Watch "+channel.getChannelName()+" Live on Fizan TV\n https://play.google.com/store/apps/details?id=com.pacificlive.tv");
//            }
//        });

        mBtn.setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ViewGestureListener listener = new ViewGestureListener(this, this);
        mGestureDetector = new GestureDetector(this,listener);
        mAudioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        mMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mScreen.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mCurVolume = -1;
                mCurBrightness = -1;
                mGestureDetector.onTouchEvent(motionEvent);
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    if(isControlsVisible){
                        hideControlsAutomatically();
                    }
                }
                return false;
            }
        });
        helper = new PreferencesHelper(this);
        playVideo(getIntent().getStringExtra("link"));

        if(helper.isAdmob()){
            loadAdMob();
        }else{
            if(helper.shouldShowPlayerInterstitial()){
                requestInterstitialAd(helper.getInterstitialUnit(), 0);
            }else if(helper.shouldShowPlayerAd()){
                requestNativeAd(helper.getPlayerAdUnit(), 0);
            }
        }

    }

    private void reportChannel(final String channel){
        AndroidNetworking.post("http://ufmo.asia/reporting.php")
                .addBodyParameter("channel",channel)
                .addBodyParameter("status","Not working")
                .setTag("Firebase")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {}
                    @Override
                    public void onError(ANError error) {}
                });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    void requestNativeAd(String id, final int retryCount){
        if(retryCount<2){
            final NativeAd nativeAd = new NativeAd(this, id);
            nativeAd.setAdListener(new NativeAdListener() {
                @Override
                public void onMediaDownloaded(Ad ad) {
                    // Native ad finished downloading all assets
                    Log.e(TAG, "Native ad finished downloading all assets.");
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    loadAdMob();
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    // Native ad is loaded and ready to be displayed
                    Log.d(TAG, "Native ad is loaded and ready to be displayed!");
                    inflateNativeAd(nativeAd);
                }

                @Override
                public void onAdClicked(Ad ad) {
                    // Native ad clicked
                    Log.d(TAG, "Native ad clicked!");
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    // Native ad impression
                    Log.d(TAG, "Native ad impression logged!");
                }
            });

            // Request an ad
            nativeAd.loadAd();
        }else{
            //requestInterstitialAd("154577565377261_204625680372449", 0);
        }
    }

    void requestInterstitialAd(String id, final int retryCount){
        if(retryCount<2){
            interstitialAd = new InterstitialAd(this, id);
            interstitialAd.setAdListener(new InterstitialAdListener() {
                @Override
                public void onInterstitialDisplayed(Ad ad) {

                }

                @Override
                public void onInterstitialDismissed(Ad ad) {

                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    requestInterstitialAd(helper.getInterstitialUnit1(), retryCount+1);
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    interstitialAd.show();
                }

                @Override
                public void onAdClicked(Ad ad) {

                }

                @Override
                public void onLoggingImpression(Ad ad) {

                }
            });
            interstitialAd.loadAd();
        }else{

        }
    }

    void inflateNativeAd(final NativeAd nativeAd){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.native_ad_dialog, null);

        dialogView.findViewById(R.id.ad_unit).setVisibility(View.VISIBLE);
        LinearLayout adChoicesContainer = dialogView.findViewById(R.id.ad_choices_container);
        AdChoicesView adChoicesView = new AdChoicesView(PlayerActivity.this, nativeAd, true);
        adChoicesContainer.addView(adChoicesView, 0);
        // Create native UI using the ad metadata.
        AdIconView nativeAdIcon = dialogView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = dialogView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = dialogView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = dialogView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = dialogView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = dialogView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = dialogView.findViewById(R.id.native_ad_call_to_action);

                // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

                // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                dialogView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);

        builder.setView(dialogView);
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        View close = dialogView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if(!isFinishing()){
            dialog.show();
        }
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    void playVideo(String url){
            loadAsWebView(url);
    }

//    void loadAsStream() {
//        webView.setVisibility(View.GONE);
//        playerView.setVisibility(VISIBLE);
//        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
//        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
//                Util.getUserAgent(this, "Fizan TV"), bandwidthMeter);
//        HlsMediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
//                .setPlaylistParser(
//                        new FilteringManifestParser<>(
//                                new HlsPlaylistParser(), null))
//                .createMediaSource(Uri.parse(mChannelList.get(channelPosition).getLink()));
//        player.prepare(videoSource);
//        player.addListener(new Player.EventListener() {
//            @Override
//            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
//
//            }
//
//            @Override
//            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
//
//            }
//
//            @Override
//            public void onLoadingChanged(boolean isLoading) {
//                if(isLoading){
//                    mProgress.setVisibility(View.VISIBLE);
//                }else{
//                    mProgress.setVisibility(View.GONE);
//                }
//            }
//
//            @Override
//            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//                switch (playbackState){
//                    case SimpleExoPlayer.STATE_BUFFERING:{
//                        mProgress.setVisibility(View.VISIBLE);
//                    }case SimpleExoPlayer.STATE_READY:{
//                        mProgress.setVisibility(View.GONE);
//                    }
//                }
//            }
//
//            @Override
//            public void onRepeatModeChanged(int repeatMode) {
//
//            }
//
//            @Override
//            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
//
//            }
//
//            @Override
//            public void onPlayerError(ExoPlaybackException error) {
//                    if(retryCount>5){
//                        if(!PlayerActivity.this.isFinishing()){
//                            error.printStackTrace();
//                            reportChannel(mChannelList.get(channelPosition).getChannelName());
//                            showError();
//                        }
//                    }else{
//                        mProgress.setVisibility(View.VISIBLE);
//                        player.setPlayWhenReady(true);
//                        retryCount++;
//                    }
//            }
//
//            @Override
//            public void onPositionDiscontinuity(int reason) {
//
//            }
//
//            @Override
//            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
//
//            }
//
//            @Override
//            public void onSeekProcessed() {
//
//            }
//        });
//        player.setPlayWhenReady(true);
//    }

    void showError(){
        AlertDialog dialog = new AlertDialog.Builder(PlayerActivity.this).setTitle("Trouble in loading stream")
                .setMessage("There's some trouble in loading stream. Please hold on. It will be fixed shortly!")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PlayerActivity.this.finish();
                    }
                })
                .setCancelable(false)
                .create();
        dialog.show();
    }

    void loadAsWebView(String url){
        webView.setInitialScale(1);
        webView.setWebChromeClient(new MyWebChromeClient());
        webView.getSettings().setAllowFileAccess(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 16) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setBackgroundColor(Color.BLACK);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        try{
            ViewGroup viewParent = (ViewGroup) upperPanel.getParent();
            viewParent.removeView(lowerPanel);
            viewParent.removeView(mScreen);
            viewParent.removeView(upperPanel);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        webView.loadUrl(url);
    }


//    void postDelayAd(){
//        new android.os.Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        AdRequest adRequest1 = new AdRequest.Builder().build();
//                        mInterstitialAd.loadAd(adRequest1);
//                        mInterstitialAd.setAdListener(new AdListener() {
//                            @Override
//                            public void onAdLoaded() {
//                                super.onAdLoaded();
//                                mInterstitialAd.show();
//                            }
//
//                            @Override
//                            public void onAdClosed() {
//                                super.onAdClosed();
//                                hideSystemUI();
//                            }
//                        });
//                    }
//                });
//                postDelayAd();
//            }
//        }, 900000*2);
//    }

    @Override
    public void onPause() {
        super.onPause();
        pause();
    }
    @Override
    public void onResume(){
        super.onResume();
        resume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn:{
                if(isPlaying){
                    pause();
                    mBtn.setImageResource(R.drawable.play);
                    isPlaying = false;
                }else{
                    resume();
                    mBtn.setImageResource(R.drawable.pause);
                    isPlaying = true;
                }
                break;
            }case R.id.screen:{
                if(!isControlsVisible){
                    showControls();
                    isControlsVisible = true;
                }else{
                    hideControls();
                    isControlsVisible = false;
                }
            }
            break;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ViewGroup parentViewGroup = (ViewGroup)webView.getParent();
        parentViewGroup.removeAllViews();
        webView.loadUrl("about:blank");
        webView.stopLoading();
        webView.setWebChromeClient(null);
        webView.setWebViewClient(null);
        webView.destroy();
        webView = null;
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
    }


    @Override
    public void onVerticalScroll(float percent, int direction) {
        if(ViewGestureListener.SWIPE_LEFT == direction){
            updateBrightness(percent);
        }else if(ViewGestureListener.SWIPE_RIGHT == direction){
            updateVolume(percent);
        }
    }

    private class MyWebChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    mProgress.setVisibility(INVISIBLE);
                } else {
                    mProgress.setVisibility(VISIBLE);
                }
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage cm) {
            // Spit out lots of console messages here
            return(true);
        }
    }

    private class MyWebViewClient extends WebViewClient{

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request){
            view.loadUrl(request.getUrl().toString());
            return true;
        }
    }

    public void pause(){
//        if(isM3U8(mChannelList.get(channelPosition).getLink())){
//            player.setPlayWhenReady(false);
//        }else {
            try {
                Class.forName("android.webkit.WebView")
                        .getMethod("onPause", (Class[]) null)
                        .invoke(webView, (Object[]) null);
            } catch(Exception ex) {
                ex.printStackTrace();
            }
//        }

    }
    public void resume(){
//        if(isM3U8(mChannelList.get(channelPosition).getLink())){
//            player.setPlayWhenReady(true);
//        }else {
            try {
                Class.forName("android.webkit.WebView")
                        .getMethod("onResume", (Class[]) null)
                        .invoke(webView, (Object[]) null);
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
//    }

    private void updateBrightness(float percent) {
        if(mCenterLayout.getVisibility() == INVISIBLE){
            mCenterLayout.setVisibility(VISIBLE);
        }
        mBrightness.setVisibility(VISIBLE);
        mControlsIcon.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.brightness,null));
        if (mCurBrightness == -1) {
            mCurBrightness = this.getWindow().getAttributes().screenBrightness;
            if (mCurBrightness <= 0.01f) {
                mCurBrightness = 0.01f;
            }
        }

        //mCenterLayout.setVisibility(VISIBLE);

        WindowManager.LayoutParams attributes = this.getWindow().getAttributes();
        attributes.screenBrightness = mCurBrightness + percent;
        if (attributes.screenBrightness >= 1.0f) {
            attributes.screenBrightness = 1.0f;
        } else if (attributes.screenBrightness <= 0.01f) {
            attributes.screenBrightness = 0.01f;
        }
        this.getWindow().setAttributes(attributes);

        float p = attributes.screenBrightness * 100;
        mBrightness.setProgress((int) p);
        mControlsPer.setText(mBrightness.getProgress()+"");
        //hideControlsAutomatically();
    }

    private void updateVolume(float percent) {

        if(mCenterLayout.getVisibility() == INVISIBLE){
            mCenterLayout.setVisibility(VISIBLE);
        }
        mControlsIcon.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.volume,null));
        mVolume.setVisibility(VISIBLE);

        if (mCurVolume == -1) {
            mCurVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            if (mCurVolume < 0) {
                mCurVolume = 0;
            }
        }

        int volume = (int) (percent * mMaxVolume) + mCurVolume;
        if (volume > mMaxVolume) {
            volume = mMaxVolume;
        }

        if (volume < 0) {
            volume = 0;
        }
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);

        int progress = volume * 100 / mMaxVolume;
        mVolume.setProgress(progress);
        mControlsPer.setText(mVolume.getProgress()+"");
        //hideControlsAutomatically();
    }

    public void showControls(){
        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down_u);
        slide_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                upperPanel.setVisibility(VISIBLE);
                lowerPanel.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        upperPanel.startAnimation(slide_down);
        lowerPanel.startAnimation(slide_up);
    }
    public void hideControls(){
        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        slide_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                upperPanel.setVisibility(INVISIBLE);
                lowerPanel.setVisibility(INVISIBLE);
                if(mBrightness.getVisibility() == VISIBLE){
                    mBrightness.setVisibility(INVISIBLE);
                }
                if(mVolume.getVisibility() == VISIBLE){
                    mVolume.setVisibility(INVISIBLE);
                }
                if(mCenterLayout.getVisibility() == VISIBLE){
                    mCenterLayout.setVisibility(INVISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up_u);
        upperPanel.startAnimation(slide_up);
        lowerPanel.startAnimation(slide_down);
    }

    public void hideControlsAutomatically(){
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isControlsVisible){
                    isControlsVisible = false;
                    hideControls();
                }
            }
        },4000);
    }

    public boolean isM3U8(String s){
        return s.contains("m3u8");
    }

    void loadAdMob(){
        interstitialAd1.loadAd(new AdRequest.Builder().build());
    }
}

