package com.elite.fizantv;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdListener;
import com.elite.fizantv.Utilities.PreferencesHelper;
import com.elite.fizantv.Utilities.Utils;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONArray;
import org.json.JSONObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;


public class Splash extends AppCompatActivity {

    private static String TAG = "Splash";
    PreferencesHelper mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_1);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .build();
        AndroidNetworking.initialize(this, okHttpClient);
        mPreferences = new PreferencesHelper(this);
        checkInfo();
    }

    private void startMainActivity() {
        startActivity(new Intent(Splash.this,MainActivity.class));
        Splash.this.finish();
    }

    void checkInfo(){
        AndroidNetworking.get(Utils.VERSION)
                .setTag("Splash")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String stringResponse) {
                        try {
                            JSONObject response = new JSONObject(stringResponse);
                            PackageInfo pInfo = Splash.this.getPackageManager().getPackageInfo(getPackageName(), 0);
                            int versionCode = pInfo.versionCode;
                            int vCode = response.getInt("version_code");
                            if (vCode > versionCode) {
                                showUpdateDialog(response.getString("url"));
                            } else if (response.has("ad_unit")) {
                                JSONArray adUnits = response.getJSONArray("ad_unit");
                                for (int i = 0; i < adUnits.length(); i++) {
                                    JSONObject adObject = adUnits.getJSONObject(i);
                                    String key = adObject.getString("name");

                                    if (key.equals("player_native")) {
                                        mPreferences.setShouldShowPlayerAd(adObject.getInt("enable") == 1);
                                        mPreferences.setPlayerAdUnit(adObject.getString("unit_id"));
                                    } else if (key.equals("home_banner")) {
                                        mPreferences.setBannerAdUnit(adObject.getString("unit_id"));
                                    } else if (key.equals("home_native")) {
                                        mPreferences.setShouldShowHomeAd(adObject.getInt("enable") == 1);
                                        mPreferences.setHomeAdUnit(adObject.getString("unit_id"));
                                    } else if (key.equals("news_native")) {
                                        mPreferences.setShouldShowSportsAd(adObject.getInt("enable") == 1);
                                        mPreferences.setSportAdUnit(adObject.getString("unit_id"));
                                    } else if (key.equals("player_interstitial")) {
                                        mPreferences.setShouldShowPlayerInterstitial(adObject.getInt("enable") == 1);
                                        mPreferences.setInterstitialAdUnit(adObject.getString("unit_id"));
                                    } else if (key.equals("player_interstitial_alt")) {
                                        mPreferences.setShouldShowPlayerInterstitial(adObject.getInt("enable") == 1);
                                        mPreferences.setInterstitialAdUnit(adObject.getString("unit_id"));
                                    } else if (key.equals("admob")) {
                                        mPreferences.setAdmob(adObject.getInt("enable") == 1);
                                    }
                                }
                                startMainActivity();
                            } else {
                                mPreferences.setShouldShowPlayerAd(false);
                                mPreferences.setShouldShowHomeAd(false);
                                mPreferences.setShouldShowSportsAd(false);
                                mPreferences.setShouldShowPlayerInterstitial(true);

                                mPreferences.setInterstitialAdUnit("587052361741089_587055901740735");
                                mPreferences.setBannerAdUnit("587052361741089_587053715074287");
                                mPreferences.setAdmob(false);
                                startMainActivity();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        mPreferences.setShouldShowPlayerAd(false);
                        mPreferences.setShouldShowHomeAd(false);
                        mPreferences.setShouldShowSportsAd(false);
                        mPreferences.setShouldShowPlayerInterstitial(false);

                        mPreferences.setInterstitialAdUnit("154577565377261_178544256313925");
                        mPreferences.setBannerAdUnit("154577565377261_174984510003233");
                        mPreferences.setAdmob(false);

                        startMainActivity();
                    }
            });
    }

    private void showUpdateDialog(final String url) {
        new MaterialStyledDialog.Builder(this)
                .setTitle("Fizan TV")
                .setDescription("This version of Fizan TV is outdated. Please update Fizan TV to latest version to enjoy streaming.")
                .setIcon(R.drawable.app_icon)
                .setHeaderColor(R.color.colorPrimary)
                .setPositiveText("Update")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + url)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + url)));
                        }
                    }
                })
                .setCancelable(false)
                .show();
    }
    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("Splash", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("Splash", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("Splash", "printHashKey()", e);
        }
    }

    private NativeAd nativeAd;

    private void loadNativeAd() {
        // Instantiate a NativeAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        nativeAd = new NativeAd(this, "154577565377261_219548398880177");
        nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                inflateAd(nativeAd);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
                Log.d(TAG, "Native ad impression logged!");
            }
        });
        nativeAd.loadAd();
    }

    private void inflateAd(NativeAd nativeAd) {

        nativeAd.unregisterView();

        RelativeLayout adView = findViewById(R.id.activity_splash);

        // Add the AdChoices icon
        LinearLayout adChoicesContainer = findViewById(R.id.ad_choices_container);
        AdChoicesView adChoicesView = new AdChoicesView(this, nativeAd, true);
        adChoicesContainer.addView(adChoicesView, 0);

        // Create native UI using the ad metadata.
        AdIconView nativeAdIcon = findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = findViewById(R.id.native_ad_media);
        TextView nativeAdBody = findViewById(R.id.native_ad_body);
        Button nativeAdCallToAction = findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
    }
}